package org.gcube.portal.ddas;

import java.net.URL;
import java.time.Instant;

public class BrokerResponse {
	private int status;
	private String message;
	private long timeStamp;
	private URL vreConfirmUrl;
	
	public BrokerResponse() {
	}

	public BrokerResponse(int status, String message, URL vreConfirmUrl) {
		super();
		this.status = status;
		this.message = message;
		this.vreConfirmUrl = vreConfirmUrl;
		Instant instant = Instant.now();
		this.timeStamp = instant.toEpochMilli();
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public URL getVreConfirmUrl() {
		return vreConfirmUrl;
	}

	public void setVreConfirmUrl(URL vreConfirmUrl) {
		this.vreConfirmUrl = vreConfirmUrl;
	}

	@Override
	public String toString() {
		return "BrokerResponse [status=" + status + ", message=" + message + ", timeStamp=" + timeStamp
				+ ", vreConfirmUrl=" + vreConfirmUrl + "]";
	}

	
}
