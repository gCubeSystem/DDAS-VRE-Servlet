package org.gcube.portal.ddas;



import java.io.BufferedReader;
import java.io.IOException;
import java.net.URL;
import java.util.Base64;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import net.spy.memcached.MemcachedClient;
import net.spy.memcached.internal.OperationFuture;

/**
 * Servlet implementation class DdasVREService
 */
public class DdasVREService extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static com.liferay.portal.kernel.log.Log _log = LogFactoryUtil.getLog(DdasVREService.class);  
	private static final int CACHE_SECONDS_EXPIRATION = 1800; //30 minutes
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DdasVREService() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String toReturn = "<DIV>Blue-Cloud VRE - DD&amp;AS integration accepts POST method only</DIV>";
		response.setContentType("text/html");		
		response.getWriter().write(toReturn); 
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StringBuffer sb = new StringBuffer();
		String line = null;
		JsonObject jsonObject = null;
		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			_log.debug("GOT String: "+sb.toString());
			JsonParser parser = new JsonParser();
			jsonObject = parser.parse(sb.toString()).getAsJsonObject();
		} catch (Exception e) {
			_log.error("Error parsing JSON request string", e);
		}
		Gson gson = new Gson();
		BrokerResponse theResponse = null;
		String requestId = null;
		try {
			requestId = jsonObject.get("requestId").getAsString();
			if (requestId == null || requestId.equals("")) {
				theResponse = new BrokerResponse(405, "Method Not Allowed, something is wrong in the JSON, request_id not found", null);
			}
		} catch (NullPointerException ex) {
			_log.error("Error parsing JSON request string, request_id paarm not found");
			theResponse = new BrokerResponse(405, "Method Not Allowed, something is wrong in the JSON, request_id not found", null);
			String toReturn = gson.toJson(theResponse); 
			response.setContentType("application/json");		
			response.getWriter().write(toReturn); 
			return;
		}

		_log.debug("GOT JSON");
		_log.debug("JSON="+jsonObject.toString());

		String otp = UUID.randomUUID().toString();
		boolean result = authorizeRequest(otp, jsonObject);
		if (result) {
			int status = 200;
			Base64.Encoder withoutPaddingEncoder = Base64.getEncoder().withoutPadding();
			String requestIdParam  =  withoutPaddingEncoder.encodeToString("otp".getBytes());
			String message = "Started downloading order files to Virtual Research Environment '"+requestId+"'";
			String vreCallBackURL = BrokerServiceEndpoint.getVRECallbackURLFromServiceEndpoint();
			String vreConfirmUrl = new StringBuilder(vreCallBackURL).append("?").append(requestIdParam).append("=").append(otp).toString(); //redirect URL
			URL theURL = new URL(vreConfirmUrl);
			theResponse = new BrokerResponse(status, message, theURL);
			_log.debug("Response to DD&AS BROKER:\n" +theResponse.toString());
		} 
		else {
			theResponse = new BrokerResponse(500, "An error occurred in the VRE Service, downlaod not possible", null);
		}

		String toReturn = gson.toJson(theResponse); 
		response.setContentType("application/json");		
		response.getWriter().write(toReturn); 
	}

	/**
	 * the method writes the request JSON in the memcached for @see CACHE_SECONDS_EXPIRATION seconds
	 * @param requestId
	 * @param jsonObject
	 * @return true if the operations is successful
	 */
	private boolean authorizeRequest(String expiry_token,JsonObject jsonObject) {
		OperationFuture<Boolean> writeOp = null;
		try {
			MemcachedClient mClient = new DistributedCacheClient().getMemcachedClient();
			writeOp = mClient.set(expiry_token, CACHE_SECONDS_EXPIRATION, jsonObject.toString());		
			Thread.sleep(500);
		} catch (Exception e) {
			e.printStackTrace();
			return writeOp.getStatus().isSuccess();
		} 
		return writeOp.getStatus().isSuccess();
	}

}
