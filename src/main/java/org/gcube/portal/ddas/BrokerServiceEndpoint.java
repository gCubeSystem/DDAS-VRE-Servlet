package org.gcube.portal.ddas;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.util.List;

import org.gcube.common.portal.PortalContext;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.slf4j.LoggerFactory;

/**
 * @author Massimiliano Assante at ISTI-CNR 
 */
public class BrokerServiceEndpoint {

	// Logger
	private static final org.slf4j.Logger _log = LoggerFactory.getLogger(BrokerServiceEndpoint.class);


	
	private static final String BC_BROKER_RESOURCE_NAME = "Blue-Cloud-DataDownloadAndAccess";
	private static final String CATEGORY_NAME = "Service";
	private static final String CALLBACK_PROPERTY_NAME = "vre_callback_url";
	
	/**
	 * Retrieve the vre callbac URL to pass to the broker service in the response from the endpoint resource on IS 
	 * @throws Exception
	 */
	public static String getVRECallbackURLFromServiceEndpoint(){

		String currentScope = ScopeProvider.instance.get();
		String infrastructure = "/"+PortalContext.getConfiguration().getInfrastructureName();
		ScopeProvider.instance.set(infrastructure);
		try{
			SimpleQuery query = queryFor(ServiceEndpoint.class);
			query.addCondition("$resource/Profile/Name/text() eq '"+ BC_BROKER_RESOURCE_NAME +"'");
			query.addCondition("$resource/Profile/Category/text() eq '"+ CATEGORY_NAME +"'");
			DiscoveryClient<ServiceEndpoint> client = clientFor(ServiceEndpoint.class);
			try {
				List<ServiceEndpoint> list = client.submit(query);
				if (list.size() > 1) {
					System.out.println("Too many Service Endpoints having name " + BC_BROKER_RESOURCE_NAME +" in this scope having Category " + CATEGORY_NAME);
				}
				else if (list.size() == 0){
					System.out.println("There is no Service Endpoint having name " + BC_BROKER_RESOURCE_NAME +" and Category " + CATEGORY_NAME + " in this context " + infrastructure);
				}
				else {
					for (ServiceEndpoint res : list) {
						AccessPoint[] accessPoints = (AccessPoint[]) res.profile().accessPoints().toArray(new AccessPoint[res.profile().accessPoints().size()]);
						for (AccessPoint found : accessPoints) {
							for (ServiceEndpoint.Property prop : found.properties()) {
								System.out.println(prop.name());
								if (prop.name().compareTo(CALLBACK_PROPERTY_NAME) == 0)
									return prop.value();											
							}			
						}					
						_log.warn("There is no Service Endpoint having name: " + BC_BROKER_RESOURCE_NAME + " and Category " + CATEGORY_NAME + " on root context");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch(Exception e){
			_log.error("There is no Service Endpoint having name: " + BC_BROKER_RESOURCE_NAME + " and Category " + CATEGORY_NAME + " on root context");
		}finally{
			ScopeProvider.instance.set(currentScope);
		}
		ScopeProvider.instance.set(currentScope);
		return null;
	}

}
